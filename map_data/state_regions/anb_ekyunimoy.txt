﻿STATE_ESIMOINE = {
    id = 270
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x02FD3B" "x02FD84" "x19F81E" "x1F5555" "x1F55C5" "x22F6CA" "x2B9230" "x37B0C4" "x397FCF" "x43591B" "x55453C" "x564A37" "x567637" "x625A03" "x78B789" "x7C70AB" "x86E3B5" "x9AA4B9" "x9BC4D6" "xA45452" "xA5E729" "xA71D2B" "xA8029D" "xA810C9" "xA86DC9" "xB02050" "xB22178" "xC9478C" "xCE75F6" "xD43A4F" "xD7C0DF" "xE32C31" "xE8124F" "xE98C0B" "xE9FFED" "xEABEF5" }
    traits = { state_trait_ekyunimoy_range }
    city = "x02fd3b" #Random
    farm = "x78b789" #Random
    wood = "xb02050" #Random
    mine = "xa810c9" #Random
    arable_land = 50
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_logging = 3
        bg_coal_mining = 22
    }
    resource = {
        type = "bg_gold_fields"
        depleted_type = "bg_gold_mining"
        undiscovered_amount = 10
    }
}
STATE_MORGANIA = {
    id = 271
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x0962C2" "x1828CA" "x4B542E" "x57A085" "x7A4665" "x7B3847" "x851A82" "x87C05C" "x8A65F0" "xA89044" "xADB202" "xB1895F" "xD05A2E" "xD312AC" "xD3F738" "xD493AE" "xDD52B4" "xEB2460" "xF00190" "xF0E0B3" "xFABD16" "xFE90A7" "xFFEEFC" }
    traits = { state_trait_ekyunimoy_range state_trait_harafe_desert }
    city = "xa45452" #Random
    farm = "xa5e729" #Random
    wood = "x99af78" #Random
    mine = "xb8a52a" #Random
    arable_land = 50
    arable_resources = { bg_livestock_ranches }
}
STATE_TASPAS = {
    id = 272
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x0CC25B" "x0EE70B" "x1080D0" "x1803D0" "x18C0CB" "x1F1368" "x21B59A" "x245E2A" "x39D7C8" "x413E51" "x455C7F" "x50ADEE" "x528D86" "x5ECE79" "x8C9A5C" "x9068BD" "x9D4993" "xAA1982" "xC71E45" "xC9C900" "xCCAD63" "xCE1B54" "xF39243" "xF42C38" }
    traits = { state_trait_ekyunimoy_range }
    city = "x39d7c8" #Random
    farm = "x9d4993" #Random
    wood = "x528d86" #Random
    mine = "x1803d0" #Random
    arable_land = 50
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_logging = 2
        bg_iron_mining = 33
    }
}
STATE_EKRSOKA = {
    id = 273
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x1620A9" "x1C63CA" "x1DB8ED" "x478C17" "x519820" "x529C7E" "x569559" "x6FFD2D" "x73CD08" "x8113CC" "x835A64" "xB06090" "xC5D272" "xD196AD" "xD19A4F" "xD19A99" "xD1F150" "xF508CE" }
    traits = {}
    city = "xcc4a4f" #Random
    farm = "x9b5562" #Random
    wood = "x0b75a6" #Random
    mine = "xa77cd4" #Random
    arable_land = 50
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_logging = 4
        bg_coal_mining = 31
    }
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 25
    }
}