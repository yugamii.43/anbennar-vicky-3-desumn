﻿je_settle_the_folly = {
	icon = "gfx/interface/icons/event_icons/event_map.dds"

	complete = {
		country_or_subject_owns_entire_state_region = STATE_DREADMIRE
		country_or_subject_owns_entire_state_region = STATE_CORVELD
		country_or_subject_owns_entire_state_region = STATE_FENNFORT
	}
	
	on_complete = {
		# TODO
	}
	
	# Invalid if you don't own anything in the folly
	fail = {
		NOT = { has_state_in_state_region = STATE_DREADMIRE }
		NOT = { has_state_in_state_region = STATE_CORVELD }
		NOT = { has_state_in_state_region = STATE_FENNFORT }
	}
	
	on_fail = {
		if = {
			limit = {
				has_claim = s:STATE_DREADMIRE
			}
			
			s:STATE_DREADMIRE = {
				remove_claim = root
			}
		}

		if = {
			limit = {
				has_claim = s:STATE_CORVELD
			}

			s:STATE_CORVELD = {
				remove_claim = root
			}
		}

		if = {
			limit = {
				has_claim = s:STATE_FENNFORT
			}
			
			s:STATE_FENNFORT = {
				remove_claim = root
			}
		}
	}

	# Own anything in the folly and have colonial affairs
	possible = {
		OR = {
			has_state_in_state_region = STATE_DREADMIRE
			has_state_in_state_region = STATE_CORVELD
			has_state_in_state_region = STATE_FENNFORT
		}
		
		institution_investment_level = {
			institution = institution_colonial_affairs
			value >= 1
		}
	}

	is_shown_when_inactive = {
		OR = {
			has_state_in_state_region = STATE_DREADMIRE
			has_state_in_state_region = STATE_CORVELD
			has_state_in_state_region = STATE_FENNFORT
		}
	}
	
	on_weekly_pulse = {
		events = {
		}
	}

	should_be_pinned_by_default = yes

	weight = 100
	
	can_revolution_inherit = yes
}
