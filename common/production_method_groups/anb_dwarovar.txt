﻿

#Serpentbloom Farms
pmg_base_building_serpentbloom_farm = {
	production_methods = {
		pm_simple_farming
		pm_soil_enriching_farming
		pm_fertilization
		pm_chemical_fertilizer
	}
}
pmg_secondary_building_serpentbloom_farm = {
	production_methods = {
		pm_no_secondary
		pm_serpentbloom_opium
	}
}
pmg_harvesting_process_building_serpentbloom_farm = {
	production_methods = {
		pm_tools_disabled
		pm_tools
		pm_steam_threshers
		pm_tractors_building_rye_farm
		pm_compression_ignition_tractors_building_rye_farm
	}
}
pmg_ownership_land_building_serpentbloom_farm = {
	production_methods = {
		pm_privately_owned
		pm_publicly_traded
		pm_homesteading
		pm_government_run
		pm_worker_cooperative_farm
	}
}


#Mushroom Farms
pmg_base_building_mushroom_farm = {
	production_methods = {
		pm_simple_farming
		pm_soil_enriching_farming
		pm_fertilization
		pm_chemical_fertilizer
	}
}
pmg_secondary_building_mushroom_farm = {
	production_methods = {
		pm_no_secondary
		pm_mushroom_liquor
	}
}
pmg_harvesting_process_building_mushroom_farm = {
	production_methods = {
		pm_tools_disabled
		pm_tools
		pm_steam_threshers
		pm_tractors_building_rye_farm
		pm_compression_ignition_tractors_building_rye_farm
	}
}
pmg_ownership_land_building_mushroom_farm = {
	production_methods = {
		pm_privately_owned
		pm_publicly_traded
		pm_homesteading
		pm_government_run
		pm_worker_cooperative_farm
	}
}

#Cave Coral
pmg_base_building_cave_coral = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_simple_forestry_cave_coral
		pm_saw_mills_cave_coral
		pm_electric_saw_mills_cave_coral
	}
}
pmg_hardwood_cave_coral = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_refining.dds"
	production_methods = {
		pm_no_hardwood
		pm_hardwood_cave_coral
		pm_increased_hardwood_cave_coral
	}
}
pmg_equipment_cave_coral = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_automation.dds"
	production_methods = {
		pm_no_equipment
		pm_steam_donkey_building_logging_camp
		pm_chainsaws
	}
}
pmg_transportation_building_cave_coral = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_automation.dds"
	production_methods = {
		pm_road_carts
		pm_rail_transport_building_logging_camp
		pm_log_carts
	}
}
pmg_ownership_capital_building_cave_coral = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_ownership.dds"
	production_methods = {
		pm_merchant_guilds_building_cave_coral
		pm_privately_owned_building_cave_coral
		pm_publicly_traded_building_cave_coral
		pm_government_run_building_cave_coral
		pm_worker_cooperative_building_cave_coral
	}
}

#Mithril Mine
pmg_mining_equipment_building_mithril_mine = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_picks_and_shovels_building_mithril_mine
		pm_atmospheric_engine_pump_building_mithril_mine
		pm_condensing_engine_pump_building_mithril_mine
		pm_diesel_pump_building_mithril_mine
	}
}
pmg_explosives_building_mithril_mine = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_no_explosives
		pm_nitroglycerin_building_mithril_mine
		pm_dynamite_building_mithril_mine
	}
}
pmg_steam_automation_building_mithril_mine = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_automation.dds"
	production_methods = {
		pm_no_steam_automation
		pm_steam_donkey_mine
	}
}
pmg_train_automation_building_mithril_mine = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_automation.dds"
	production_methods = {
		pm_road_carts
		pm_rail_transport_mine
	}
}
pmg_ownership_capital_building_mithril_mine = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_ownership.dds"
	production_methods = {
		pm_merchant_guilds_building_mithril_mine
		pm_privately_owned_building_mithril_mine
		pm_publicly_traded_building_mithril_mine
		pm_government_run_building_mithril_mine
		pm_worker_cooperative_building_mithril_mine
	}
}

#Gem Mine
pmg_mining_equipment_building_gem_mine = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_picks_and_shovels_building_gem_mine
		pm_atmospheric_engine_pump_building_gem_mine
		pm_condensing_engine_pump_building_gem_mine
		pm_diesel_pump_building_gem_mine
	}
}
pmg_steam_automation_building_gem_mine = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_automation.dds"
	production_methods = {
		pm_no_steam_automation
		pm_steam_donkey_mine
	}
}
pmg_train_automation_building_gem_mine = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_automation.dds"
	production_methods = {
		pm_road_carts
		pm_rail_transport_mine
	}
}
pmg_ownership_capital_building_gem_mine = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_ownership.dds"
	production_methods = {
		pm_merchant_guilds_building_gem_mine
		pm_privately_owned_building_gem_mine
		pm_publicly_traded_building_gem_mine
		pm_government_run_building_gem_mine
		pm_worker_cooperative_building_gem_mine
	}
}