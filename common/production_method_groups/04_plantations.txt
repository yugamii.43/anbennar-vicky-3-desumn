﻿pmg_base_building_coffee_plantation = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		default_building_coffee_plantation
		conjurative_irrigation_coffee_plantation # Anbennar
		automatic_irrigation_building_coffee_plantation
	}
}

pmg_enhancements_coffee_plantation = { # Anbennar
	texture = "gfx/interface/icons/generic_icons/mixed_icon_refining.dds"
	production_methods = {
		pm_no_enhancements # Anbennar
		pm_mass_cast_plant_growth_coffee_plantation # Anbennar
		pm_spirit_imbuement_coffee_plantation # Anbennar
		pm_growth_serum_coffee_plantation # Anbennar
	}
}

pmg_train_automation_building_coffee_plantation = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_automation.dds"
	production_methods = {
		pm_road_carts
		pm_steam_rail_transport
	}
}

pmg_ownership_land_building_coffee_plantation = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_ownership.dds"
	production_methods = {
		pm_privately_owned_plantation
		pm_publicly_traded_plantation
		pm_government_run_plantation
		pm_worker_cooperative_plantation
	}
}

pmg_base_building_cotton_plantation = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		default_building_cotton_plantation
		conjurative_irrigation_cotton_plantation # Anbennar
		automatic_irrigation_building_cotton_plantation
	}
}

pmg_enhancements_cotton_plantation = { # Anbennar
	texture = "gfx/interface/icons/generic_icons/mixed_icon_refining.dds"
	production_methods = {
		pm_no_enhancements # Anbennar
		pm_mass_cast_plant_growth_cotton_plantation # Anbennar
		pm_spirit_imbuement_cotton_plantation # Anbennar
		pm_growth_serum_cotton_plantation # Anbennar
	}
}

pmg_train_automation_building_cotton_plantation = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_automation.dds"
	production_methods = {
		pm_road_carts
		pm_steam_rail_transport
	}
}

pmg_ownership_land_building_cotton_plantation = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_ownership.dds"
	production_methods = {
		pm_privately_owned_plantation
		pm_publicly_traded_plantation
		pm_government_run_plantation
		pm_worker_cooperative_plantation
	}
}

pmg_base_building_dye_plantation = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		default_building_dye_plantation
		conjurative_irrigation_dye_plantation # Anbennar
		automatic_irrigation_building_dye_plantation
	}
}

pmg_enhancements_dye_plantation = { # Anbennar
	texture = "gfx/interface/icons/generic_icons/mixed_icon_refining.dds"
	production_methods = {
		pm_no_enhancements # Anbennar
		pm_mass_cast_plant_growth_dye_plantation # Anbennar
		pm_spirit_imbuement_dye_plantation # Anbennar
		pm_growth_serum_dye_plantation # Anbennar
	}
}

pmg_train_automation_building_dye_plantation = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_automation.dds"
	production_methods = {
		pm_road_carts
		pm_steam_rail_transport
	}
}

pmg_ownership_land_building_dye_plantation = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_ownership.dds"
	production_methods = {
		pm_privately_owned_plantation
		pm_publicly_traded_plantation
		pm_government_run_plantation
		pm_worker_cooperative_plantation
	}
}

pmg_base_building_opium_plantation = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		default_building_opium_plantation
		conjurative_irrigation_opium_plantation # Anbennar
		automatic_irrigation_building_opium_plantation
	}
}

pmg_enhancements_opium_plantation = { # Anbennar
	texture = "gfx/interface/icons/generic_icons/mixed_icon_refining.dds"
	production_methods = {
		pm_no_enhancements # Anbennar
		pm_mass_cast_plant_growth_opium_plantation # Anbennar
		pm_spirit_imbuement_opium_plantation # Anbennar
		pm_growth_serum_opium_plantation # Anbennar
	}
}

pmg_train_automation_building_opium_plantation = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_automation.dds"
	production_methods = {
		pm_road_carts
		pm_steam_rail_transport
	}
}

pmg_ownership_land_building_opium_plantation = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_ownership.dds"
	production_methods = {
		pm_privately_owned_plantation
		pm_publicly_traded_plantation
		pm_government_run_plantation
		pm_worker_cooperative_plantation
	}
}

pmg_base_building_tea_plantation = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		default_building_tea_plantation
		conjurative_irrigation_tea_plantation # Anbennar
		automatic_irrigation_building_tea_plantation
	}
}

pmg_enhancements_tea_plantation = { # Anbennar
	texture = "gfx/interface/icons/generic_icons/mixed_icon_refining.dds"
	production_methods = {
		pm_no_enhancements # Anbennar
		pm_mass_cast_plant_growth_tea_plantation # Anbennar
		pm_spirit_imbuement_tea_plantation # Anbennar
		pm_growth_serum_tea_plantation # Anbennar
	}
}

pmg_train_automation_building_tea_plantation = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_automation.dds"
	production_methods = {
		pm_road_carts
		pm_steam_rail_transport
	}
}

pmg_ownership_land_building_tea_plantation = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_ownership.dds"
	production_methods = {
		pm_privately_owned_plantation
		pm_publicly_traded_plantation
		pm_government_run_plantation
		pm_worker_cooperative_plantation
	}
}

pmg_base_building_tobacco_plantation = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		default_building_tobacco_plantation
		conjurative_irrigation_tobacco_plantation # Anbennar
		automatic_irrigation_building_tobacco_plantation
	}
}

pmg_enhancements_tobacco_plantation = { # Anbennar
	texture = "gfx/interface/icons/generic_icons/mixed_icon_refining.dds"
	production_methods = {
		pm_no_enhancements # Anbennar
		pm_mass_cast_plant_growth_tobacco_plantation # Anbennar
		pm_spirit_imbuement_tobacco_plantation # Anbennar
		pm_growth_serum_tobacco_plantation # Anbennar
	}
}

pmg_train_automation_building_tobacco_plantation = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_automation.dds"
	production_methods = {
		pm_road_carts
		pm_steam_rail_transport
	}
}

pmg_ownership_land_building_tobacco_plantation = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_ownership.dds"
	production_methods = {
		pm_privately_owned_plantation
		pm_publicly_traded_plantation
		pm_government_run_plantation
		pm_worker_cooperative_plantation
	}
}

pmg_base_building_sugar_plantation = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		default_building_sugar_plantation
		conjurative_irrigation_sugar_plantation # Anbennar
		automatic_irrigation_building_sugar_plantation
	}
}

pmg_enhancements_sugar_plantation = { # Anbennar
	texture = "gfx/interface/icons/generic_icons/mixed_icon_refining.dds"
	production_methods = {
		pm_no_enhancements # Anbennar
		pm_mass_cast_plant_growth_sugar_plantation # Anbennar
		pm_spirit_imbuement_sugar_plantation # Anbennar
		pm_growth_serum_sugar_plantation # Anbennar
	}
}

pmg_train_automation_building_sugar_plantation = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_automation.dds"
	production_methods = {
		pm_road_carts
		pm_steam_rail_transport
	}
}

pmg_ownership_land_building_sugar_plantation = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_ownership.dds"
	production_methods = {
		pm_privately_owned_plantation
		pm_publicly_traded_plantation
		pm_government_run_plantation
		pm_worker_cooperative_plantation
	}
}

pmg_base_building_banana_plantation = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		default_building_banana_plantation
		conjurative_irrigation_banana_plantation # Anbennar
		automatic_irrigation_building_banana_plantation
	}
}

pmg_enhancements_banana_plantation = { # Anbennar
	texture = "gfx/interface/icons/generic_icons/mixed_icon_refining.dds"
	production_methods = {
		pm_no_enhancements # Anbennar
		pm_mass_cast_plant_growth_fruit_plantation # Anbennar
		pm_spirit_imbuement_fruit_plantation # Anbennar
		pm_growth_serum_fruit_plantation # Anbennar
	}
}

pmg_train_automation_building_banana_plantation = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_automation.dds"
	production_methods = {
		pm_road_carts
		pm_steam_rail_transport
	}
}

pmg_ownership_land_building_banana_plantation = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_ownership.dds"
	production_methods = {
		pm_privately_owned_plantation
		pm_publicly_traded_plantation
		pm_government_run_plantation
		pm_worker_cooperative_plantation
	}
}

pmg_base_building_silk_plantation = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		default_building_silk_plantation
		conjurative_irrigation_silk_plantation # Anbennar
		automatic_irrigation_building_silk_plantation
	}
}

pmg_enhancements_silk_plantation = { # Anbennar
	texture = "gfx/interface/icons/generic_icons/mixed_icon_refining.dds"
	production_methods = {
		pm_no_enhancements # Anbennar
		pm_mass_cast_plant_growth_silk_plantation # Anbennar
		pm_spirit_imbuement_silk_plantation # Anbennar
		pm_growth_serum_silk_plantation # Anbennar
	}
}

pmg_train_automation_building_silk_plantation = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_automation.dds"
	production_methods = {
		pm_road_carts
		pm_steam_rail_transport
	}
}

pmg_ownership_land_building_silk_plantation = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_ownership.dds"
	production_methods = {
		pm_privately_owned_plantation
		pm_publicly_traded_plantation
		pm_government_run_plantation
		pm_worker_cooperative_plantation
	}
}

pmg_base_building_vineyard_plantation = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		default_building_vineyard_plantation
		conjurative_irrigation_vineyard_plantation # Anbennar
		automatic_irrigation_building_vineyard_plantation
	}
}

pmg_enhancements_vineyard_plantation = { # Anbennar
	texture = "gfx/interface/icons/generic_icons/mixed_icon_refining.dds"
	production_methods = {
		pm_no_enhancements # Anbennar
		pm_mass_cast_plant_growth_vineyard_plantation # Anbennar
		pm_spirit_imbuement_vineyard_plantation # Anbennar
		pm_growth_serum_vineyard_plantation # Anbennar
	}
}

pmg_train_automation_building_vineyard_plantation = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_automation.dds"
	production_methods = {
		pm_road_carts
		pm_steam_rail_transport
	}
}

pmg_ownership_land_building_vineyard_plantation = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_ownership.dds"
	production_methods = {
		pm_privately_owned_plantation
		pm_publicly_traded_plantation
		pm_government_run_plantation
		pm_worker_cooperative_plantation
	}
}