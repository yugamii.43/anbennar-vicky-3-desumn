﻿A69 = { #Esmaria
	use_culture_states = yes
	
	is_major_formation = yes
	
	unification_play = dp_unify_esmaria
	leadership_play = dp_leadership_esmaria

	required_states_fraction = 0.7
	
	ai_will_do = { always = yes }

	possible = {
		any_country = {
			OR = {
				country_has_primary_culture = cu:esmari
			}
			has_technology_researched = nationalism
		}
	}
}