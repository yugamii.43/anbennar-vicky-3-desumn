﻿POPS = {
	s:STATE_AVHAVUBHIYA = {
		region_state:R01 = {
			create_pop = {
				culture = raghamidesh
				size = 6400000
			}
			create_pop = {
				culture = royal_harimari
				size = 370000
				religion = the_jadd
			}
			create_pop = {
				culture = sun_elven
				size = 70000
			}
			create_pop = {
				culture = siadunan_harpy
				size = 150000
			}
			create_pop = {
				culture = peridot_dwarf
				size = 70000
			}
			create_pop = {
				culture = sandfang_gnoll
				size = 370000
			}

		}
	}
	s:STATE_IYARHASHAR = {
		region_state:R01 = {
			create_pop = {
				culture = raghamidesh
				size = 1560000
			}
			create_pop = {
				culture = royal_harimari
				size = 1410000
				split_religion = {
					royal_harimari = {
						the_jadd = 0.9
						high_philosophy = 0.1
					}
				}
			}
			create_pop = {
				culture = sun_elven
				size = 30000
			}
			create_pop = {
				culture = sandfang_gnoll
				size = 60000
			}

		}
	}
	s:STATE_SOUTH_GHANKEDHEN = {
		region_state:R02 = {
			create_pop = {
				culture = ghankedhen
				size = 4030000
			}
			create_pop = {
				culture = royal_harimari
				size = 100000
				religion = the_jadd
			}
			create_pop = {
				culture = sun_elven
				size = 50000
			}
			create_pop = {
				culture = siadunan_harpy
				size = 350000
			}
			create_pop = {
				culture = peridot_dwarf
				size = 450000
			}

		}
	}
	s:STATE_NORTH_GHANKEDHEN = {
		region_state:R02 = {
			create_pop = {
				culture = ghankedhen
				size = 2220000
			}
			create_pop = {
				culture = royal_harimari
				size = 130000
				religion = the_jadd
			}
			create_pop = {
				culture = sun_elven
				size = 30000
			}
			create_pop = {
				culture = siadunan_harpy
				size = 360000
			}
			create_pop = {
				culture = peridot_dwarf
				size = 560000
			}

		}
	}
	s:STATE_TUDHINA = {
		region_state:R03 = {
			create_pop = {
				culture = pasindesh
				size = 1020000
			}
			create_pop = {
				culture = royal_harimari
				size = 70000
			}

		}
	}
	s:STATE_PASIRAGHA = {
		region_state:R01 = {
			create_pop = {
				culture = pasindesh
				size = 10020000
			}
			create_pop = {
				culture = royal_harimari
				size = 3270000
				split_religion = {
					royal_harimari = {
						the_jadd = 0.9
						high_philosophy = 0.1
					}
				}
			}
			create_pop = {
				culture = sun_elven
				size = 70000
			}
			create_pop = {
				culture = siadunan_harpy
				size = 300000
			}
			create_pop = {
				culture = peridot_dwarf
				size = 1190000
			}

		}
	}
	s:STATE_UPPER_DHENBASANA = {
		region_state:R01 = {
			create_pop = {
				culture = rabhidarubsad
				size = 11770000
				split_religion = {
					rabhidarubsad = {
						the_jadd = 0.9
						high_philosophy = 0.1
					}
				}
			}
			create_pop = {
				culture = royal_harimari
				size = 8280000
				split_religion = {
					royal_harimari = {
						the_jadd = 0.9
						high_philosophy = 0.1
					}
				}
			}
			create_pop = {
				culture = sun_elven
				size = 220000
			}
			create_pop = {
				culture = siadunan_harpy
				size = 1090000
			}
			create_pop = {
				culture = peridot_dwarf
				size = 440000
			}

		}
	}
	s:STATE_LOWER_DHENBASANA = {
		region_state:R01 = {
			create_pop = {
				culture = dhukharuved
				size = 12420000
				split_religion = {
					dhukharuved = {
						the_jadd = 0.8
						high_philosophy = 0.2
					}
				}
			}
			create_pop = {
				culture = royal_harimari
				size = 5590000
				split_religion = {
					royal_harimari = {
						the_jadd = 0.8
						high_philosophy = 0.2
					}
				}
			}
			create_pop = {
				culture = sun_elven
				size = 20000
			}

		}
	}
	s:STATE_ASCENSION_JUNGLE = {
		region_state:R04 = {
			create_pop = {
				culture = raghamidesh
				size = 650000
			}
			create_pop = {
				culture = royal_harimari
				size = 1850000
			}
		}
	}
	s:STATE_TUJGAL = {
		region_state:R04 = {
			create_pop = {
				culture = sarniryabsad
				size = 290000
			}
			create_pop = {
				culture = royal_harimari
				size = 1390000
			}

		}
	}
	s:STATE_BABHAGAMA = {
		region_state:R06 = {
			create_pop = {
				culture = sobhagand
				size = 580000
			}
			create_pop = {
				culture = royal_harimari
				size = 230000
			}

		}
	}
	s:STATE_SATARSAYA = {
		region_state:R01 = {
			create_pop = {
				culture = rabhidarubsad
				size = 3430000
				split_religion = {
					rabhidarubsad = {
						the_jadd = 0.8
						high_philosophy = 0.2
					}
				}
			}
			create_pop = {
				culture = royal_harimari
				size = 470000
				split_religion = {
					royal_harimari = {
						the_jadd = 0.8
						high_philosophy = 0.2
					}
				}
			}

		}
	}
	s:STATE_WEST_GHAVAANAJ = {
		region_state:R72 = {
			create_pop = {
				culture = ghavaanaj
				size = 2460000
			}
			create_pop = {
				culture = royal_harimari
				size = 30000
			}
			create_pop = {
				culture = tiger_hobgoblin
				size = 50000
			}

		}
	}
	s:STATE_EAST_GHAVAANAJ = {
		region_state:R72 = {
			create_pop = {
				culture = azepyanunin
				size = 3590000
			}
			create_pop = {
				culture = royal_harimari
				size = 90000
			}
			create_pop = {
				culture = tiger_hobgoblin
				size = 910000
			}

		}
		region_state:R12 = {
			create_pop = {
				culture = azepyanunin
				size = 970000
			}
			create_pop = {
				culture = royal_harimari
				size = 140000
			}
		}
	}
	s:STATE_TUGHAYASA = {
		region_state:R09 = {
			create_pop = {
				culture = azepyanunin
				size = 970000
			}
			create_pop = {
				culture = royal_harimari
				size = 120000
			}
			create_pop = {
				culture = elephant_hobgoblin
				size = 10000
			}
		}
		region_state:R13 = {
			create_pop = {
				culture = azepyanunin
				size = 2500000
			}
			create_pop = {
				culture = royal_harimari
				size = 30000
			}
			create_pop = {
				culture = elephant_hobgoblin
				size = 100000
			}
		}
	}
	s:STATE_DHUJAT = {
		region_state:R10 = {
			create_pop = {
				culture = rasarhid
				size = 9050000
			}
			create_pop = {
				culture = royal_harimari
				size = 3250000
			}
			create_pop = {
				culture = sun_elven
				size = 30000
			}
			create_pop = {
				culture = tiger_hobgoblin
				size = 80000
			}

		}
		region_state:R11 = {
			create_pop = {
				culture = rasarhid
				size = 3440000
			}
			create_pop = {
				culture = royal_harimari
				size = 1410000
			}
			create_pop = {
				culture = sun_elven
				size = 20000
			}
			create_pop = {
				culture = tiger_hobgoblin
				size = 100000
			}
		}
	}
	s:STATE_SARISUNG = {
		region_state:R13 = {
			create_pop = {
				culture = azepyanunin
				size = 14860000
			}
			create_pop = {
				culture = royal_harimari
				size = 2380000
			}
			create_pop = {
				culture = sun_elven
				size = 40000
			}
			create_pop = {
				culture = elephant_hobgoblin
				size = 2380000
			}

		}
	}
	s:STATE_TILTAGHAR = {
		region_state:R07 = {
			create_pop = {
				culture = rajnadhid
				size = 770000
			}
			create_pop = {
				culture = royal_harimari
				size = 410000
			}
			create_pop = {
				culture = tiger_hobgoblin
				size = 30000
			}
			create_pop = {
				culture = wuhyun_half_orc
				size = 60000
			}

		}
	}
	s:STATE_WEST_NADIMRAJ = {
		region_state:R07 = {
			create_pop = {
				culture = shandibad
				size = 1430000
			}
			create_pop = {
				culture = royal_harimari
				size = 150000
			}
			create_pop = {
				culture = tiger_hobgoblin
				size = 70000
			}

		}
		region_state:D26 = {
			create_pop = {
				culture = shandibad
				size = 500000
			}
			create_pop = {
				culture = royal_harimari
				size = 70000
			}

		}
	}
	s:STATE_RAJNADHAGA = {
		region_state:R07 = {
			create_pop = {
				culture = shandibad
				size = 5370000
			}
			create_pop = {
				culture = royal_harimari
				size = 740000
			}
			create_pop = {
				culture = tiger_hobgoblin
				size = 270000
			}
			create_pop = {
				culture = wuhyun_half_orc
				size = 340000
			}
			create_pop = {
				culture = peridot_dwarf
				size = 30000
			}

		}
	}
	s:STATE_CENTRAL_NADIMRAJ = {
		region_state:R07 = {
			create_pop = {
				culture = azepyanunin
				size = 3850000
			}
			create_pop = {
				culture = royal_harimari
				size = 950000
			}
			create_pop = {
				culture = tiger_hobgoblin
				size = 830000
			}
			create_pop = {
				culture = wuhyun_half_orc
				size = 300000
			}
			create_pop = {
				culture = peridot_dwarf
				size = 20000
			}

		}
	}
	s:STATE_EAST_NADIMRAJ = {
		region_state:R07 = {
			create_pop = {
				culture = azepyanunin
				size = 3280000
			}
			create_pop = {
				culture = royal_harimari
				size = 1180000
			}
			create_pop = {
				culture = tiger_hobgoblin
				size = 410000
			}
			create_pop = {
				culture = wuhyun_half_orc
				size = 260000
			}

		}
	}
	s:STATE_HOBGOBLIN_HOMELANDS = {
		region_state:R08 = {
			create_pop = {
				culture = azepyanunin
				size = 310000
			}
			create_pop = {
				culture = boar_hobgoblin
				size = 3400000
			}
			create_pop = {
				culture = march_goblin
				size = 360000
			}
			create_pop = {
				culture = wuhyun_half_orc
				size = 400000
			}

		}
	}
	s:STATE_GHILAKHAD = {
		region_state:R14 = {
			create_pop = {
				culture = azepyanunin
				size = 1010000
			}
			create_pop = {
				culture = royal_harimari
				size = 170000
			}
			create_pop = {
				culture = wolf_hobgoblin
				size = 90000
			}
			create_pop = {
				culture = wuhyun_half_orc
				size = 440000
			}

		}
	}
	s:STATE_RAGHAJANDI = {
		region_state:R08 = {
			create_pop = {
				culture = azepyanunin
				size = 4760000
			}
			create_pop = {
				culture = royal_harimari
				size = 460000
			}
			create_pop = {
				culture = wolf_hobgoblin
				size = 4180000
			}
			create_pop = {
				culture = march_goblin
				size = 700000
			}
			create_pop = {
				culture = wuhyun_half_orc
				size = 1510000
			}

		}
	}
	s:STATE_GHATASAK = {
		region_state:R17 = {
			create_pop = {
				culture = azepyanunin
				size = 780000
			}
			create_pop = {
				culture = royal_harimari
				size = 90000
			}
			create_pop = {
				culture = lion_hobgoblin
				size = 780000
			}
			create_pop = {
				culture = wuhyun_half_orc
				size = 200000
			}

		}
	}
	s:STATE_SHAMAKHAD_PLAINS = {
		region_state:R15 = {
			create_pop = {
				culture = azepyanunin
				size = 890000
			}
			create_pop = {
				culture = royal_harimari
				size = 50000
			}
			create_pop = {
				culture = wolf_hobgoblin
				size = 130000
			}
			create_pop = {
				culture = march_goblin
				size = 80000
			}
			create_pop = {
				culture = wuhyun_half_orc
				size = 560000
			}

		}
		region_state:R16 = {
			create_pop = {
				culture = azepyanunin
				size = 1390000
			}
			create_pop = {
				culture = royal_harimari
				size = 230000
			}
			create_pop = {
				culture = wolf_hobgoblin
				size = 450000
			}
			create_pop = {
				culture = march_goblin
				size = 180000
			}
			create_pop = {
				culture = wuhyun_half_orc
				size = 330000
			}

		}
		region_state:R18 = {
			create_pop = {
				culture = azepyanunin
				size = 3700000
			}
			create_pop = {
				culture = royal_harimari
				size = 440000
			}
			create_pop = {
				culture = wolf_hobgoblin
				size = 760000
			}
			create_pop = {
				culture = march_goblin
				size = 260000
			}
			create_pop = {
				culture = wuhyun_half_orc
				size = 870000
			}
		}
	}
	s:STATE_SIR = {
		region_state:R19 = {
			create_pop = {
				culture = azepyanunin
				size = 9890000
			}
			create_pop = {
				culture = east_harimari
				size = 320000
			}
			create_pop = {
				culture = dragon_hobgoblin
				size = 3890000
			}
			create_pop = {
				culture = march_goblin
				size = 650000
			}
			create_pop = {
				culture = wuhyun_half_orc
				size = 1460000
			}

		}
	}
	s:STATE_SRAMAYA = {
		region_state:R05 = {
			create_pop = {
				culture = sarniryabsad
				size = 13660000
			}
			create_pop = {
				culture = royal_harimari
				size = 890000
			}
			create_pop = {
				culture = creek_gnome
				size = 30000
			}
			create_pop = {
				culture = peridot_dwarf
				size = 640000
			}

		}
		region_state:Y32 = {
			create_pop = {
				culture = sarniryabsad
				size = 810000
			}
			create_pop = {
				culture = royal_harimari
				size = 80000
			}
		}
	}
}