﻿POPS = {
	s:STATE_NOGRUD = {
		region_state:C03 = {
			create_pop = {
				culture = ozgar_orc
				size = 228092
			}
			create_pop = {
				culture = soruinic
				size = 33460
			}
			create_pop = {
				culture = effe_i
				size = 38800
			}
		}
	}
	s:STATE_KARLUR_DARAKH = {
		region_state:C14 = {
			create_pop = {
				culture = seedthrall
				size = 764996
			}
			create_pop = {
				culture = effe_i
				size = 75000
			}
		}
	}
	s:STATE_YARUHOL = {
		region_state:C03 = {
			create_pop = {
				culture = ozgar_orc
				size = 353250
			}
			create_pop = {
				culture = soruinic
				size = 18225
			}
			create_pop = {
				culture = cannorian_half_orc
				size = 3525
			}
		}
	}
	s:STATE_FASHTUG = {
		region_state:C03 = {
			create_pop = {
				culture = ozgar_orc
				size = 779890
			}
			create_pop = {
				culture = soruinic
				size = 24048
			}
			create_pop = {
				culture = cannorian_half_orc
				size = 30645
			}
		}
	}
	s:STATE_OZGAR = {
		region_state:C03 = {
			create_pop = {
				culture = ozgar_orc
				size = 629990
			}
			create_pop = {
				culture = cannorian_half_orc
				size = 73606
			}
			create_pop = {
				culture = soruinic
				size = 27010
			}
		}
	}
	s:STATE_BRAMMYAR = {
		region_state:C03 = {
			create_pop = {
				culture = ozgar_orc
				size = 703070
			}
			create_pop = {
				culture = soruinic
				size = 135057
			}
			create_pop = {
				culture = cannorian_half_orc
				size = 3873
			}
		}
	}
	s:STATE_JIBIRAEN = {
		region_state:C04 = {
			create_pop = {
				culture = soruinic
				size = 191738
			}
			create_pop = {
				culture = ozgar_orc
				size = 10262
			}
		}
	}
	s:STATE_MARUKHAN = {
		region_state:C03 = {
			create_pop = {
				culture = ozgar_orc
				size = 160176
			}
			create_pop = {
				culture = parura
				size = 48777
			}
			create_pop = {
				culture = leechman
				size = 4047
			}
		}
	}
	s:STATE_THE_MIDDANS = {
		region_state:C06 = {
			create_pop = {
				culture = swampman
				size = 12345
			}
			create_pop = {
				culture = leechman
				size = 16900
			}
			create_pop = {
				culture = parura
				size = 9876
			}
		}
		region_state:A08 = {
			create_pop = {
				culture = tefori
				size = 21200
			}
			create_pop = {
				culture = parura
				size = 15900
			}
			create_pop = {
				culture = leechman
				size = 10800
			}
		}
	}
	s:STATE_SCREAMING_JUNGLE = {
		region_state:C12 = {
			create_pop = {
				culture = parura
				size = 27200
			}
			create_pop = {
				culture = leechman
				size = 4800
			}
		}
		region_state:C13 = {
			create_pop = {
				culture = parura
				size = 27000
			}
			create_pop = {
				culture = leechman
				size = 5000
			}
		}
	}
	s:STATE_WESTERN_EFFELAI = {
		region_state:C14 = {
			create_pop = {
				culture = seedthrall
				size = 720000
			}
			create_pop = {
				culture = effe_i
				size = 80000
			}
		}
		region_state:C15 = {
			create_pop = {
				culture = effe_i
				size = 60000
			}
			create_pop = {
				culture = lai_i
				size = 20000
			}
		}
	}
	s:STATE_MUSHROOM_FOREST = {
		region_state:C16 = {
			create_pop = {
				culture = lai_i
				size = 20600
			}
			create_pop = {
				culture = dugui_hi #Replace with mushroom culture once it exists
				size = 82400
			}
		}
	}
	s:STATE_CENTRAL_EFFELAI = {
		region_state:C14 = {
			create_pop = {
				culture = seedthrall
				size = 800000
			}
			create_pop = {
				culture = parura
				size = 50000
			}
		}
		region_state:C06 = {
			create_pop = {
				culture = swampman
				size = 100000
			}
			create_pop = {
				culture = parura
				size = 50000
			}
		}
	}
	s:STATE_EASTERN_EFFELAI = {
		region_state:C14 = {
			create_pop = {
				culture = seedthrall
				size = 562000
			}
			create_pop = {
				culture = thavo_i
				size = 65000
			}
			create_pop = {
				culture = lai_i
				size = 12300
			}
		}
	}
	s:STATE_SORFEN = {
		region_state:C06 = {
			create_pop = {
				culture = swampman
				size = 210000
			}
			create_pop = {
				culture = leechman
				size = 28000
			}
			create_pop = {
				culture = parura
				size = 42000
			}
		}
	}
	s:STATE_SEINAINE = {
		region_state:C05 = {
			create_pop = {
				culture = dhanaenno
				size = 264590
			}
			create_pop = {
				culture = parura
				size = 29540
			}
			create_pop = {
				culture = green_orc
				size = 72670
			}
		}
		region_state:C06 = {
			create_pop = {
				culture = swampman
				size = 30200
			}
			create_pop = {
				culture = leechman
				size = 8000
			}
			create_pop = {
				culture = parura
				size = 2100
			}
		}
		region_state:A01 = {
			create_pop = {
				culture = pearlsedger
				size = 1350
			}
		}
	}
	s:STATE_DALAINE = {
		region_state:C05 = {
			create_pop = {
				culture = dhanaenno
				size = 304590
			}
			create_pop = {
				culture = thavo_i
				size = 42900
			}
			create_pop = {
				culture = green_orc
				size = 47190
			}
			create_pop = {
				culture = imperial_gnome
				size = 4290
			}
			create_pop = {
				culture = beefoot_halfling
				size = 30030
			}
		}
	}
	s:STATE_DEEPSONG = {
		region_state:C17 = {
			create_pop = {
				culture = thavo_i
				size = 16200
			}
		}
		region_state:C14 = {
			create_pop = {
				culture = seedthrall
				size = 145800
			}
		}
	}
	s:STATE_THALASARAN = {
		region_state:C08 = {
			create_pop = {
				culture = drakesman
				size = 150960
			}
			create_pop = {
				culture = thavo_i
				size = 14280
			}
			create_pop = {
				culture = green_orc
				size = 22400
			}
		}
		region_state:A03 = {
			create_pop = {
				culture = roilsardi
				size = 16320
			}
			create_pop = {
				culture = thavo_i
				size = 1000
			}
		}
	}
	s:STATE_KIOHALEN = {
		region_state:C07 = {
			create_pop = {
				culture = kioha_harpy
				size = 376800
			}
			create_pop = {
				culture = thavo_i
				size = 125600
			}
			create_pop = {
				culture = drakesman
				size = 125600
			}
		}
	}
	s:STATE_KIINDTIR = {
		region_state:C07 = {
			create_pop = {
				culture = kioha_harpy
				size = 239850
			}
			create_pop = {
				culture = thavo_i
				size = 55350
			}
			create_pop = {
				culture = drakesman
				size = 73800
			}
		}
		region_state:C01 = {
			create_pop = {
				culture = kioha_harpy
				size = 6100
			}
			create_pop = {
				culture = oono_i
				size = 2340
			}
		}
	}
	s:STATE_DAZINKOST = {
		region_state:C01 = {
			create_pop = {
				culture = dawn_elf
				size = 40120
			}
			create_pop = {
				culture = rezankandish
				size = 22420
			}
			create_pop = {
				culture = kioha_harpy
				size = 5900
			}
			create_pop = {
				culture = oono_i #Replace with Dawn Ruinborn once those exist
				size = 49560
			}
		}
	}
	s:STATE_REZANOAN = {
		region_state:C01 = {
			create_pop = {
				culture = dawn_elf
				size = 149500
			}
			create_pop = {
				culture = rezankandish
				size = 106750
			}
			create_pop = {
				culture = oono_i
				size = 149400
			}
			create_pop = {
				culture = kioha_harpy
				size = 21350
			}
		}
	}
	s:STATE_BROAN_KEIR = {
		region_state:C01 = {
			create_pop = {
				culture = dawn_elf
				size = 79800
			}
			create_pop = {
				culture = rezankandish
				size = 45600
			}
			create_pop = {
				culture = oono_i
				size = 88920
			}
		}
	}
	s:STATE_NUREL = {
		region_state:C01 = {
			create_pop = {
				culture = dawn_elf
				size = 99400
			}
			create_pop = {
				culture = rezankandish
				size = 56800
			}
			create_pop = {
				culture = oono_i
				size = 105080
			}
			create_pop = {
				culture = kalavendhi
				size = 17040
				religion = new_sun_cult
			}
			create_pop = {
				culture = lai_i
				size = 5680
				religion = new_sun_cult
			}
		}
	}
	s:STATE_ARENEL = {
		region_state:C01 = {
			create_pop = {
				culture = dawn_elf
				size = 161100
			}
			create_pop = {
				culture = rezankandish
				size = 107400
			}
			create_pop = {
				culture = oono_i
				size = 187950
			}
			create_pop = {
				culture = kalavendhi
				size = 53700
				religion = new_sun_cult
			}
			create_pop = {
				culture = lai_i
				size = 26850
				religion = new_sun_cult
			}
		}
	}
	s:STATE_NUR_ELIZNA = {
		region_state:C01 = {
			create_pop = {
				culture = dawn_elf
				size = 57600
			}
			create_pop = {
				culture = rezankandish
				size = 44800
			}
			create_pop = {
				culture = oono_i
				size = 12800
			}
			create_pop = {
				culture = kalavendhi
				size = 12800
				religion = new_sun_cult
			}
		}
	}
	s:STATE_NEWSHORE = {
		region_state:C10 = {
			create_pop = {
				culture = malateli
				size = 205700
				split_religion = {
					malateli = {
						ravelian = 0.75
						new_sun_cult = 0.25
					}
				}
			}
			create_pop = {
				culture = dawn_elf
				size = 12100
			}
			create_pop = {
				culture = rezankandish
				size = 24200
			}
		}
	}
	s:STATE_TIMBERNECK = {
		region_state:C10 = {
			create_pop = {
				culture = malateli
				size = 170100
				split_religion = {
					malateli = {
						ravelian = 0.8
						new_sun_cult = 0.2
					}
				}
			}
			create_pop = {
				culture = dawn_elf
				size = 7560
			}
			create_pop = {
				culture = rezankandish
				size = 17640
			}
		}
	}
	s:STATE_VRENDIN = {
		region_state:C10 = {
			create_pop = {
				culture = malateli
				size = 176400
				split_religion = {
					malateli = {
						ravelian = 0.85
						new_sun_cult = 0.15
					}
				}
			}
			create_pop = {
				culture = thekvrystana
				size = 50400
				split_religion = {
					thekvrystana = {
						gods_of_the_taychend = 0.6
						ravelian = 0.1
						new_sun_cult = 0.3
					}
				}
			}
			create_pop = {
				culture = dawn_elf
				size = 7560
			}
			create_pop = {
				culture = rezankandish
				size = 17640
			}
		}
		region_state:A01 = {
			create_pop = {
				culture = pearlsedger
				size = 4320
			}
			create_pop = {
				culture = thekvrystana
				size = 3210
			}
		}
	}
	s:STATE_LAIPOINT_ARCHIPELAGO = {
		region_state:A01 = {
			create_pop = {
				culture = pearlsedger
				size = 7395
			}
		}
		region_state:A08 = {
			create_pop = {
				culture = tefori
				size = 7105
			}
		}
	}
	s:STATE_WEST_TURTLEBACK = {
		region_state:C09 = {
			create_pop = {
				culture = busilari
				size = 177600
			}
			create_pop = {
				culture = vernman
				size = 199800
			}
			create_pop = {
				culture = green_orc
				size = 66600
			}
		}
	}
	s:STATE_EAST_TURTLEBACK = {
		region_state:C09 = {
			create_pop = {
				culture = busilari
				size = 194250
			}
			create_pop = {
				culture = vernman
				size = 277500
			}
			create_pop = {
				culture = green_orc
				size = 55500
			}
			create_pop = {
				culture = beefoot_halfling
				size = 27750
			}
		}
	}
}