﻿COUNTRIES = {
	c:B24 ?= {
		effect_starting_technology_tier_2_tech = yes

		add_technology_researched = egalitarianism

		effect_starting_politics_liberal = yes
		
		activate_law = law_type:law_parliamentary_republic
		activate_law = law_type:law_wealth_voting #Can have a seat in the assembly if you're rich, so nobles, planters and capitalists really
		activate_law = law_type:law_racial_segregation
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_hereditary_bureaucrats		
		activate_law = law_type:law_national_militia
		# No home affairs
		activate_law = law_type:law_interventionism
		activate_law = law_type:law_per_capita_based_taxation
		# No colonial affairs
		activate_law = law_type:law_local_police #To strenghten landowners a bit
		# No schools
		# No health system
		activate_law = law_type:law_artifice_encouraged #Most pro-artifice in the Trollsbay
		
		activate_law = law_type:law_right_of_assembly
		activate_law = law_type:law_tenant_farmers
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_women_own_property
		activate_law = law_type:law_poor_laws
		# No migration controls
		activate_law = law_type:law_legacy_slavery
		
		activate_law = law_type:law_non_monstrous_only

		ig:ig_industrialists = {
			add_ruling_interest_group = yes #Just overtook planters a few years ago
		}
	}
}