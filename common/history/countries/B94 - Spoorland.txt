﻿COUNTRIES = {
	c:B89 = {
		effect_starting_technology_tier_3_tech = yes

		activate_law = law_type:law_presidential_republic
		activate_law = law_type:law_census_voting
		activate_law = law_type:law_national_supremacy
		activate_law = law_type:law_state_religion
		activate_law = law_type:law_elected_bureaucrats
		activate_law = law_type:law_national_militia
		# No home affairs
		activate_law = law_type:law_agrarianism
		activate_law = law_type:law_mercantilism
		activate_law = law_type:law_land_based_taxation
		# No colonial affairs
		activate_law = law_type:law_no_migration_controls
		# No police
		# No schools
		# No health system
		activate_law = law_type:law_artifice_encouraged
		activate_law = law_type:law_homesteading
		activate_law = law_type:law_right_of_assembly
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_women_own_property
		# No social security
		# No migration controls
		activate_law = law_type:law_legacy_slavery
		
		activate_law = law_type:law_same_race_and_humans
	}
}