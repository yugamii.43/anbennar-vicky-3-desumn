﻿COUNTRIES = {
	c:B85 = {
		effect_starting_technology_tier_3_tech = yes
		
		effect_starting_politics_traditional = yes
		
		activate_law = law_type:law_theocracy
		activate_law = law_type:law_oligarchy

		activate_law = law_type:law_religious_schools
		activate_law = law_type:law_no_health_system

		activate_law = law_type:law_ruinborn_group_only
	}
}