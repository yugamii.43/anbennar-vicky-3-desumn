﻿AI = {
	c:R07 = {	#Nadiraji
		set_strategy = ai_strategy_territorial_expansion
		random_list = {
			50 = { set_strategy = ai_strategy_conservative_agenda }
			50 = { set_strategy = ai_strategy_progressive_agenda }
		}
	}
	c:R08 = {	#Old Command
		set_strategy = ai_strategy_territorial_expansion
		random_list = {
			50 = { set_strategy = ai_strategy_conservative_agenda }
			50 = { set_strategy = ai_strategy_progressive_agenda }
		}
	}	
	c:R09 = {	#Pordhatti
		set_strategy = ai_strategy_territorial_expansion
		random_list = {
			50 = { set_strategy = ai_strategy_conservative_agenda }
			50 = { set_strategy = ai_strategy_progressive_agenda }
		}
	}	
	c:R10 = {	#Khiraspid
		set_strategy = ai_strategy_territorial_expansion
		random_list = {
			50 = { set_strategy = ai_strategy_conservative_agenda }
			50 = { set_strategy = ai_strategy_progressive_agenda }
		}
	}	
	c:R11 = {	#Keyattordha
		set_strategy = ai_strategy_territorial_expansion
		random_list = {
			50 = { set_strategy = ai_strategy_conservative_agenda }
			50 = { set_strategy = ai_strategy_progressive_agenda }
		}
	}	
	c:R12 = {	#Maharaaja
		set_strategy = ai_strategy_territorial_expansion
		random_list = {
			50 = { set_strategy = ai_strategy_conservative_agenda }
			50 = { set_strategy = ai_strategy_progressive_agenda }
		}
	}	
	c:R13 = {	#Yodashikyu
		set_strategy = ai_strategy_territorial_expansion
		random_list = {
			50 = { set_strategy = ai_strategy_conservative_agenda }
			50 = { set_strategy = ai_strategy_progressive_agenda }
		}
	}	
	c:R14 = {	#Command A
		set_strategy = ai_strategy_territorial_expansion
		random_list = {
			50 = { set_strategy = ai_strategy_conservative_agenda }
			50 = { set_strategy = ai_strategy_progressive_agenda }
		}
	}	
	c:R15 = {	#Command B
		set_strategy = ai_strategy_territorial_expansion
		random_list = {
			50 = { set_strategy = ai_strategy_conservative_agenda }
			50 = { set_strategy = ai_strategy_progressive_agenda }
		}
	}	
	c:R16 = {	#Command C
		set_strategy = ai_strategy_territorial_expansion
		random_list = {
			50 = { set_strategy = ai_strategy_conservative_agenda }
			50 = { set_strategy = ai_strategy_progressive_agenda }
		}
	}	
	c:R17 = {	#Lion Command
		set_strategy = ai_strategy_territorial_expansion
		random_list = {
			50 = { set_strategy = ai_strategy_conservative_agenda }
			50 = { set_strategy = ai_strategy_progressive_agenda }
		}
	}	
	c:R18 = {	#Command E
		set_strategy = ai_strategy_territorial_expansion
		random_list = {
			50 = { set_strategy = ai_strategy_conservative_agenda }
			50 = { set_strategy = ai_strategy_progressive_agenda }
		}
	}	
	c:R19 = {	#Dragon Command	
		set_strategy = ai_strategy_territorial_expansion
		random_list = {
			50 = { set_strategy = ai_strategy_conservative_agenda }
			50 = { set_strategy = ai_strategy_progressive_agenda }
		}
	}
	c:R72 = {	#Dhugajir	
		set_strategy = ai_strategy_territorial_expansion
		random_list = {
			50 = { set_strategy = ai_strategy_conservative_agenda }
			50 = { set_strategy = ai_strategy_progressive_agenda }
		}
	}
	
}	




