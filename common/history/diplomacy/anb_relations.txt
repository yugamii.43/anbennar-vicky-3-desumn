﻿DIPLOMACY = {

	#Everyone in Escann + Anbennar dislikes magocratic demesne
	c:A30 = {
		set_relations = { country = c:A25 value = -30 }
		set_relations = { country = c:A27 value = -30 }
		set_relations = { country = c:A28 value = -30 }
		set_relations = { country = c:A29 value = -30 }
		set_relations = { country = c:A31 value = -30 }
		set_relations = { country = c:A32 value = 10 }	#Silvermere likes MD as they helped their creation
		set_relations = { country = c:A24 value = -50 }	#cos mages fucked newshire with magical farm famine
		set_relations = { country = c:A22 value = -30 }

		#Anbennar and Grombar
		set_relations = { country = c:A01 value = -50 }
		set_relations = { country = c:A10 value = -50 }
	}
	c:A30 = {	#Magocratic Demesne-Wyvernheart
		set_relations = { country = c:A26 value = 20 }
	}

	c:A22 = { #Ancardia-Silvermere
		set_relations = { country = c:A32 value = 20 }
	}
	

	c:A26 = { #Wyvernheart-Grombar
		set_relations = { country = c:A10 value = -50 }
	}

	c:A01 = { #Anbennar disliking other big dudes
		set_relations = { country = c:A02 value = -50 }
		set_relations = { country = c:A03 value = -50 }
	}

	c:A01 = { #Anbennar-Escanni relation
		set_relations = { country = c:A25 value = -50 }
		set_relations = { country = c:A26 value = -50 }
		set_relations = { country = c:A27 value = -30 }
		set_relations = { country = c:A28 value = -30 }
		set_relations = { country = c:A29 value = -30 }
		set_relations = { country = c:A30 value = -50 }
		set_relations = { country = c:A32 value = -30 }
	}

	c:A02 = {	#Vivin-Ravelian
		set_relations = { country = c:A33 value = 20 }
	}


	c:A27 = { #Blademarches-Rosande-Marrhold Three Kings Alliance
		set_relations = { country = c:A28 value = 30 }
		set_relations = { country = c:A29 value = 30 }
	}
	c:A28 = { #Blademarches-Rosande-Marrhold Three Kings Alliance
		set_relations = { country = c:A29 value = 30 }
	}

	c:A05 = { #Bisan likes their former overlord Anbennar
		set_relations = { country = c:A01 value = 20 }
	}

	c:A20 = { #Ibevar doesnt like Vivins
		set_relations = { country = c:A02 value = -20 }
	}

	c:A04 = { #Northern League-Grombar
		set_relations = { country = c:A10 value = -50 }
	}

	c:B98 = { #Trollsbay supported Ynnsmouth
		set_relations = { country = c:B21 value = 25 }
	}

	c:B42 = {	
		set_relations = { country = c:B34 value = 30 } #Former anti-Plumstead bloc
		set_relations = { country = c:B41 value = 30 } #Plumstead did a heel turn (still dislikes Beggaston though)
	}

	c:B29 = { #Anti-Havoric bloc
	set_relations = { country = c:B36 value = 30 }
	}
}
