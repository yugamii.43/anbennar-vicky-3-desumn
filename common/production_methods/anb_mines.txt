﻿## Damestear Mines
pm_merchant_guilds_building_damestear_mine = {
	texture = "gfx/interface/icons/production_method_icons/merchant_guilds.dds"

	unlocking_production_methods = {
		pm_picks_and_shovels_building_damestear_mine
	}

	disallowing_laws = {
		law_command_economy
		law_cooperative_ownership
	}

	building_modifiers = {
		level_scaled = {
			building_employment_shopkeepers_add = 500
		}
		unscaled = {
			building_shopkeepers_shares_add = 10
		}
	}
}

pm_privately_owned_building_damestear_mine = {
	texture = "gfx/interface/icons/production_method_icons/privately_owned.dds"

	unlocking_production_methods = {
		pm_atmospheric_engine_pump_building_damestear_mine
		pm_condensing_engine_pump_building_damestear_mine
		pm_diesel_pump_building_damestear_mine
	}

	disallowing_laws = {
		law_command_economy
		law_cooperative_ownership
	}

	building_modifiers = {
		level_scaled = {
			building_employment_capitalists_add = 100
		}
		unscaled = {
			building_capitalists_shares_add = 10
		}
	}
}

pm_publicly_traded_building_damestear_mine = {
	texture = "gfx/interface/icons/production_method_icons/publicly_traded.dds"

	unlocking_production_methods = {
		pm_atmospheric_engine_pump_building_damestear_mine
		pm_condensing_engine_pump_building_damestear_mine
		pm_diesel_pump_building_damestear_mine
	}

	disallowing_laws = {
		law_command_economy
		law_cooperative_ownership
	}

	unlocking_technologies = {
		mutual_funds
	}

	building_modifiers = {
		level_scaled = {
			building_employment_capitalists_add = 150
		}
		unscaled = {
			building_capitalists_shares_add = 10
		}
	}
}

pm_government_run_building_damestear_mine = {
	texture = "gfx/interface/icons/production_method_icons/government_run.dds"

	unlocking_laws = {
		law_command_economy
	}

	building_modifiers = {
		level_scaled = {
			building_employment_bureaucrats_add = 150
		}
		unscaled = {
			building_government_shares_add = 1
		}
	}
}

pm_worker_cooperative_building_damestear_mine = {
	texture = "gfx/interface/icons/production_method_icons/worker_cooperative.dds"

	unlocking_production_methods = {
		pm_atmospheric_engine_pump_building_damestear_mine
		pm_condensing_engine_pump_building_damestear_mine
		pm_diesel_pump_building_damestear_mine
	}

	unlocking_laws = {
		law_cooperative_ownership
	}

	building_modifiers = {
		level_scaled = {
			building_employment_shopkeepers_add = 250
		}
		unscaled = {
			building_workforce_shares_add = 1
		}
	}
}

pm_picks_and_shovels_building_damestear_mine = {
	texture = "gfx/interface/icons/production_method_icons/picks_and_shovels.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 5
			
			# output goods
			goods_output_damestear_add = 20 #note: since i made damestear as expensive as sulfur (most expensive vanilla raw resource), i also changed its mines to output damestear at the same rate
			# damestear is still INCREDIBLY limited, but there's now enough of it to actually have a damestear economy (hopefully)
		}

		level_scaled = {
			building_employment_laborers_add = 4500
		}
	}
}

pm_deposit_divination_damestear_mine = {
	texture = "gfx/interface/icons/production_method_icons/picks_and_shovels.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 5
			goods_input_reagents_add = 10
			
			# output goods
			goods_output_damestear_add = 40
		}

		level_scaled = {
			building_employment_laborers_add = 4500
			building_employment_mages_add = 500
		}
	}
}

pm_atmospheric_engine_pump_building_damestear_mine = {
	texture = "gfx/interface/icons/production_method_icons/pumps.dds"
	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 5
		}
	}

	unlocking_technologies = {
		atmospheric_engine
	}
	
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 10
			goods_input_coal_add = 10
			
			# output goods
			goods_output_damestear_add = 40
		}

		level_scaled = {
			building_employment_laborers_add = 4000
			building_employment_machinists_add = 500
		}
	}
}

pm_condensing_engine_pump_building_damestear_mine = {
	texture = "gfx/interface/icons/production_method_icons/condensing_engine_pump.dds"
	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 10
		}
	}

	unlocking_technologies = {
		watertube_boiler
	}
	
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 15
			goods_input_coal_add = 15
			
			# output goods
			goods_output_damestear_add = 60
		}

		level_scaled = {
			building_employment_laborers_add = 3000
			building_employment_machinists_add = 1000
			building_employment_engineers_add = 250
		}
	}
}

pm_diesel_pump_building_damestear_mine = {
	texture = "gfx/interface/icons/production_method_icons/diesel_pump.dds"
	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 15
		}
	}

	unlocking_technologies = {
		combustion_engine
	}
	
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 15
			goods_input_oil_add = 10
			
			# output goods
			goods_output_damestear_add = 80
		}

		level_scaled = {
			building_employment_laborers_add = 2000
			building_employment_machinists_add = 1500
			building_employment_engineers_add = 500
		}
	}
}

pm_transmutative_heavy_pump_damestear_mine = {
	texture ="gfx/interface/icons/production_method_icons/diesel_pump.dds"

	unlocking_technologies = {
		compression_ignition
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 15
			goods_input_oil_add = 5
			goods_input_artificery_doodads_add = 10
			
			# output goods
			goods_output_damestear_add = 95
		}

		level_scaled = {
			building_employment_laborers_add = 1500
			building_employment_machinists_add = 1750
			building_employment_engineers_add = 750
		}
	}
}

pm_evocation_spells_damestear_mine = {
	texture = "gfx/interface/icons/production_method_icons/picks_and_shovels.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_reagents_add = 7
			
			# output goods
			goods_output_damestear_add = 10
		}

		level_scaled = {
			building_employment_mages_add = 250
		}
	}
}

pm_nitroglycerin_building_damestear_mine = {
	texture = "gfx/interface/icons/production_method_icons/nitroglycerin.dds"
	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 5
		}
	}

	unlocking_technologies = {
	 	nitroglycerin
	}
	
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_explosives_add = 5
			
			# output goods
			goods_output_damestear_add = 10
		}

		level_scaled = {
			building_employment_engineers_add = 250
		}

		unscaled = {
			building_laborers_mortality_mult = 0.3
			building_machinists_mortality_mult = 0.2
			building_engineers_mortality_mult = 0.1
		}
	}
}

pm_dynamite_building_damestear_mine = {
	texture = "gfx/interface/icons/production_method_icons/dynamite.dds"
	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 10
		}
	}

	unlocking_technologies = {
		dynamite
	}
	
	building_modifiers = {
		workforce_scaled = {
			goods_input_explosives_add = 10
			
			# output goods
			goods_output_damestear_add = 20
		}

		level_scaled = {
			building_employment_engineers_add = 250
		}
	}
}

pm_target_selecting_explosions_damestear_mine = {
	texture = "gfx/interface/icons/production_method_icons/picks_and_shovels.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_explosives_add = 15
			goods_input_artificery_doodads_add = 5
			
			# output goods
			goods_output_damestear_add = 35
		}

		level_scaled = {
			building_employment_engineers_add = 250
		}
	}
}


## Damestear Fields
default_building_damestear_fields = {
	texture = "gfx/interface/icons/production_method_icons/gold_mining.dds"
	
	building_modifiers = {
		workforce_scaled = {
			goods_output_damestear_add = 20
		}
		level_scaled = {
			building_employment_shopkeepers_add = 500
			building_employment_laborers_add = 4500
		}
		unscaled = {
			building_shopkeepers_shares_add = 3
			building_laborers_shares_add = 1
		}
	}
}

## Perfect Metal Mines
pm_merchant_guilds_building_perfect_metal_mine = {
	texture = "gfx/interface/icons/production_method_icons/merchant_guilds.dds"

	unlocking_production_methods = {
		pm_picks_and_shovels_building_perfect_metal_mine
	}

	disallowing_laws = {
		law_command_economy
		law_cooperative_ownership
	}

	building_modifiers = {
		level_scaled = {
			building_employment_shopkeepers_add = 500
		}
		unscaled = {
			building_shopkeepers_shares_add = 10
		}
	}
}

pm_privately_owned_building_perfect_metal_mine = {
	texture = "gfx/interface/icons/production_method_icons/privately_owned.dds"

	unlocking_production_methods = {
		pm_atmospheric_engine_pump_building_perfect_metal_mine
		pm_condensing_engine_pump_building_perfect_metal_mine
		pm_diesel_pump_building_perfect_metal_mine
	}

	disallowing_laws = {
		law_command_economy
		law_cooperative_ownership
	}

	building_modifiers = {
		level_scaled = {
			building_employment_capitalists_add = 100
		}
		unscaled = {
			building_capitalists_shares_add = 10
		}
	}
}

pm_publicly_traded_building_perfect_metal_mine = {
	texture = "gfx/interface/icons/production_method_icons/publicly_traded.dds"

	unlocking_production_methods = {
		pm_atmospheric_engine_pump_building_perfect_metal_mine
		pm_condensing_engine_pump_building_perfect_metal_mine
		pm_diesel_pump_building_perfect_metal_mine
	}

	disallowing_laws = {
		law_command_economy
		law_cooperative_ownership
	}

	unlocking_technologies = {
		mutual_funds
	}

	building_modifiers = {
		level_scaled = {
			building_employment_capitalists_add = 150
		}
		unscaled = {
			building_capitalists_shares_add = 10
		}
	}
}

pm_government_run_building_perfect_metal_mine = {
	texture = "gfx/interface/icons/production_method_icons/government_run.dds"

	unlocking_laws = {
		law_command_economy
	}

	building_modifiers = {
		level_scaled = {
			building_employment_bureaucrats_add = 150
		}
		unscaled = {
			building_government_shares_add = 1
		}
	}
}

pm_worker_cooperative_building_perfect_metal_mine = {
	texture = "gfx/interface/icons/production_method_icons/worker_cooperative.dds"

	unlocking_production_methods = {
		pm_atmospheric_engine_pump_building_perfect_metal_mine
		pm_condensing_engine_pump_building_perfect_metal_mine
		pm_diesel_pump_building_perfect_metal_mine
	}

	unlocking_laws = {
		law_cooperative_ownership
	}

	building_modifiers = {
		level_scaled = {
			building_employment_shopkeepers_add = 250
		}
		unscaled = {
			building_workforce_shares_add = 1
		}
	}
}

pm_picks_and_shovels_building_perfect_metal_mine = {
	texture = "gfx/interface/icons/production_method_icons/picks_and_shovels.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 5
			
			# output goods
			goods_output_perfect_metal_add = 20
		}

		level_scaled = {
			building_employment_laborers_add = 4500
		}
	}
}

pm_deposit_divination_perfect_metal_mine = {
	texture = "gfx/interface/icons/production_method_icons/picks_and_shovels.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 5
			goods_input_reagents_add = 10
			
			# output goods
			goods_output_perfect_metal_add = 40
		}

		level_scaled = {
			building_employment_laborers_add = 4500
			building_employment_mages_add = 500
		}
	}
}

pm_atmospheric_engine_pump_building_perfect_metal_mine = {
	texture = "gfx/interface/icons/production_method_icons/pumps.dds"
	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 5
		}
	}

	unlocking_technologies = {
		atmospheric_engine
	}
	
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 10
			goods_input_coal_add = 10
			
			# output goods
			goods_output_perfect_metal_add = 40
		}

		level_scaled = {
			building_employment_laborers_add = 4000
			building_employment_machinists_add = 500
		}
	}
}

pm_condensing_engine_pump_building_perfect_metal_mine = {
	texture = "gfx/interface/icons/production_method_icons/condensing_engine_pump.dds"
	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 10
		}
	}

	unlocking_technologies = {
		watertube_boiler
	}
	
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 15
			goods_input_coal_add = 15
			
			# output goods
			goods_output_perfect_metal_add = 60
		}

		level_scaled = {
			building_employment_laborers_add = 3000
			building_employment_machinists_add = 1000
			building_employment_engineers_add = 250
		}
	}
}

pm_diesel_pump_building_perfect_metal_mine = {
	texture = "gfx/interface/icons/production_method_icons/diesel_pump.dds"
	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 15
		}
	}

	unlocking_technologies = {
		combustion_engine
	}
	
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 15
			goods_input_oil_add = 10
			
			# output goods
			goods_output_perfect_metal_add = 80
		}

		level_scaled = {
			building_employment_laborers_add = 2000
			building_employment_machinists_add = 1500
			building_employment_engineers_add = 500
		}
	}
}

pm_transmutative_heavy_pump_perfect_metal_mine = {
	texture = "gfx/interface/icons/production_method_icons/diesel_pump.dds"

	unlocking_technologies = {
		compression_ignition
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 15
			goods_input_oil_add = 5
			goods_input_artificery_doodads_add = 10
			
			# output goods
			goods_output_perfect_metal_add = 95
		}

		level_scaled = {
			building_employment_laborers_add = 1500
			building_employment_machinists_add = 1750
			building_employment_engineers_add = 750
		}
	}
}

pm_evocation_spells_perfect_metal_mine = {
	texture = "gfx/interface/icons/production_method_icons/picks_and_shovels.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_reagents_add = 7
			
			# output goods
			goods_output_perfect_metal_add = 10
		}

		level_scaled = {
			building_employment_mages_add = 250
		}
	}
}

pm_nitroglycerin_building_perfect_metal_mine = {
	texture = "gfx/interface/icons/production_method_icons/nitroglycerin.dds"
	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 5
		}
	}

	unlocking_technologies = {
	 	nitroglycerin
	}
	
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_explosives_add = 5
			
			# output goods
			goods_output_perfect_metal_add = 10
		}

		level_scaled = {
			building_employment_engineers_add = 250
		}

		unscaled = {
			building_laborers_mortality_mult = 0.3
			building_machinists_mortality_mult = 0.2
			building_engineers_mortality_mult = 0.1
		}
	}
}

pm_dynamite_building_perfect_metal_mine = {
	texture = "gfx/interface/icons/production_method_icons/dynamite.dds"
	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 10
		}
	}

	unlocking_technologies = {
		dynamite
	}
	
	building_modifiers = {
		workforce_scaled = {
			goods_input_explosives_add = 10
			
			# output goods
			goods_output_perfect_metal_add = 20
		}

		level_scaled = {
			building_employment_engineers_add = 250
		}
	}
}

pm_target_selecting_explosions_perfect_metal_mine = {
	texture = "gfx/interface/icons/production_method_icons/picks_and_shovels.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_explosives_add = 15
			goods_input_artificery_doodads_add = 5
			
			# output goods
			goods_output_perfect_metal_add = 35
		}

		level_scaled = {
			building_employment_engineers_add = 250
		}
	}
}

pm_sintering_forges = {
	texture = "gfx/interface/icons/production_method_icons/blister_steel_process.dds"
	
	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 10
		}
	}
}

pm_lava_pumped_forges = {
	texture = "gfx/interface/icons/production_method_icons/bessemer_process.dds"
	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 15
		}
	}
	
	unlocking_technologies = {
		bessemer_process
	}
	
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 5
			goods_input_coal_add = 10
			
			# output goods
			goods_output_perfect_metal_add = 10
		}
		unscaled = {
			building_laborers_mortality_mult = 0.05
			building_machinists_mortality_mult = 0.05
			building_engineers_mortality_mult = 0.025
		}
	}
}

pm_fire_elemental_forges = {
	texture = "gfx/interface/icons/production_method_icons/open_hearth_process.dds"
	
	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 20
		}
	}
	
	unlocking_technologies = {
		open_hearth_process
	}
	
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_reagents_add = 15
			goods_input_artificery_doodads_add = 5
			
			# output goods
			goods_output_perfect_metal_add = 25
		}
		unscaled = {
			building_laborers_mortality_mult = 0.1
			building_machinists_mortality_mult = 0.1
			building_engineers_mortality_mult = 0.05
		}
	}
}

pm_hypercharged_electric_arc_forges = {
	texture = "gfx/interface/icons/production_method_icons/electric_arc_process.dds"
	
	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 25
		}
	}
	
	unlocking_technologies = {
		electric_arc_process
	}
	
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_reagents_add = 15
			goods_input_electricity_add = 15 
			goods_input_artificery_doodads_add = 10
			
			# output goods
			goods_output_perfect_metal_add = 40
		}
		unscaled = {
			building_laborers_mortality_mult = 0.1
			building_machinists_mortality_mult = 0.1
			building_engineers_mortality_mult = 0.05
		}
	}
	required_input_goods = electricity
}

pm_dense_medium_separation = {
	texture = "gfx/interface/icons/production_method_icons/assembly_lines.dds"
	
	unlocking_technologies = {
		electrical_generation
	}
	
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_steel_add = 4
			goods_input_electricity_add = 4
		}

		level_scaled = {
			building_employment_laborers_add = -1500
		}
	}

	required_input_goods = electricity
}

pm_froth_flotation = {
	texture = "gfx/interface/icons/production_method_icons/assembly_lines.dds"
	
	unlocking_technologies = {
		conveyors
	}
	
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_electricity_add = 5
			goods_input_oil_add = 5
			goods_input_explosives_add = 5
		}

		level_scaled = {
			building_employment_laborers_add = -2000
			building_employment_machinists_add = -1000
		}
	}

	required_input_goods = electricity
}